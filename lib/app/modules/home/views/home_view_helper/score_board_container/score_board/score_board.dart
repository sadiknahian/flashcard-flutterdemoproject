import 'package:flutter/material.dart';
import 'package:voacl_lab/app/core/app_theme/app_theme.dart';
import 'package:voacl_lab/app/core/constant/string_constant/string_constant.dart';
import 'package:voacl_lab/app/modules/home/controllers/home_view_controller.dart';
import 'package:voacl_lab/app/modules/home/views/home_view_helper/todays_word_card_container/todays_word_card/solitary_data_viewer/solitary_data-viewer.dart';

import 'score_row_generator/score_row_generator.dart';

class ScoreBoard extends StatelessWidget {
  final HomeViewController controller;
  const ScoreBoard({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SolitaryDataViewer(
          title: ConstantStrings.scoreBoardText,
          isHeader: true,
        ),
        SolitaryDataViewer(
          title: ConstantStrings.topScorerOfAllTime,
          isHeader: false,
          textColor: AppColorSet.textTertiaryColor,
        ),
        SizedBox(height: 16),
        ...controller.scoreList.map(
          (e) => ScoreRowGenerator(data: e),
        ),
      ],
    );
  }
}
