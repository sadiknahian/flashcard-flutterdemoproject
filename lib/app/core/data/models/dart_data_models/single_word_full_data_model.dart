class SingleWordFullDataModel {
  int id = -1;
  String word = '';
  String meaning = '';
  String example = '';

  SingleWordFullDataModel({
    required this.id,
    required this.word,
    required this.meaning,
    required this.example,
  });

  SingleWordFullDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    word = json['word'];
    meaning = json['meaning'];
    example = json['example'];
  }

  SingleWordFullDataModel.defaultValue() {
    id = 1;
    word = 'Information';
    meaning = 'Facts provided or learned about something or someone.';
    example = 'A vital piece of information.';
  }

  SingleWordFullDataModel.defaultValue2() {
    id = 1;
    word = 'Information 2';
    meaning = 'Facts provided or learned about something or someone 2.';
    example = 'A vital piece of information 2.';
  }
}
