abstract class ConstantStrings {
  ConstantStrings._();

  static const donutChartTargetAchievementText = 'COMPLEATE / TOTAL';
  static const completedOnlineTestText = 'Test Completed';
  static const overAllScoreText = 'Master Score';
  static const lastExamScoreText = 'Last Test Score';
  static const wordOfTheDayText = 'Word Of The Day :';
  static const wordOfTheDayMeaningText = 'Meaning :';
  static const wordOfTheDayExampleText = 'Example :';
  static const scoreBoardText = 'Score Board :';
  static const topScorerOfAllTime = 'Top 10 most achiver till now ';
}
