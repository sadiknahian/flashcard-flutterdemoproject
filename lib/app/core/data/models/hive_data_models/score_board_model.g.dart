// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'score_board_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SinglePersonScoreModelAdapter
    extends TypeAdapter<SinglePersonScoreModel> {
  @override
  final int typeId = 1;

  @override
  SinglePersonScoreModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SinglePersonScoreModel(
      name: fields[1] == null ? '' : fields[1] as String,
      score: fields[2] == null ? -1 : fields[2] as double,
      position: fields[3] == null ? -1 : fields[3] as int,
    )..id = fields[0] as int;
  }

  @override
  void write(BinaryWriter writer, SinglePersonScoreModel obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.score)
      ..writeByte(3)
      ..write(obj.position);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SinglePersonScoreModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
