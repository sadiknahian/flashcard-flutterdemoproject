# voacl_lab

A new Flutter project pet project.

This is a flutter pet project with only 2 GUI. A landing page and Learn page.

for state management I use GetX, Riverpod
for local database I use Hive
for HTTP Client I use Dio


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


This is a flutter pet project with only 2 GUI. A landing page and Learn page.
-- for state management I use GetX, Riverpod
-- for local database I use Hive
-- for HTTP Client I use Dio


********************************************
