import 'package:get/get.dart';
import 'package:voacl_lab/app/modules/home/controllers/home_view_controller.dart';

class HomeViewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeViewController>(
      () => HomeViewController(),
    );
  }
}
