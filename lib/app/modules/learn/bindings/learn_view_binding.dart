import 'package:get/get.dart';
import 'package:voacl_lab/app/modules/learn/controllers/learn_view_controller.dart';

class LearnViewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LearnViewController>(
      () => LearnViewController(),
    );
  }
}
