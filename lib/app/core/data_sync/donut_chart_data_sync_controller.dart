import 'package:dartz/dartz.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/target_achivement_donut_data_model.dart';
import 'package:voacl_lab/app/core/services/api_call_helper/http_communication.dart';

Future<TargetDonutStatModel> syncDonutChartData() async {
  JSON data = getDummyJsonData();
  FailureOrJSON response = Right(data);
  return parseScoreCardData(response);
}

TargetDonutStatModel parseScoreCardData(FailureOrJSON failureOrJson) {
  TargetDonutStatModel targetDonutStatModel =
      TargetDonutStatModel.defaultValue();
  return failureOrJson.fold(
    (failure) {
      return targetDonutStatModel;
    },
    (json) {
      return saveDonutChartData(json);
    },
  );
}

TargetDonutStatModel saveDonutChartData(JSON json) {
  TargetDonutStatModel targetDonutStatModel =
      TargetDonutStatModel.defaultValue();
  for (JSON json in json['summary_data']) {
    targetDonutStatModel = TargetDonutStatModel(
      target: json['target'],
      achievement: json['achievement'],
      testCompleted: json['test_completed'],
      masterScore: json['master_score'],
      lastTestScore: json['lastTestScore'],
    );
  }
  return targetDonutStatModel;
}

JSON getDummyJsonData() {
  JSON data = {
    'summary_data': [
      {
        'target': 40,
        'achievement': 15,
        'test_completed': 5,
        'master_score': 20.8,
        'lastTestScore': 8.4,
      }
    ]
  };
  return data;
}
