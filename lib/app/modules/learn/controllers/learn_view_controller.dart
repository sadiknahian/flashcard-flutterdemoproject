import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/single_word_full_data_model.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/single_word_model.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/word_set_model.dart';
import 'package:voacl_lab/app/core/data/repository/riverpod/providers/providers.dart';
import 'package:voacl_lab/app/core/widget/snackbar/snackbar.dart';

class LearnViewController extends GetxController {
  RxBool isLoading = true.obs;

  late WidgetRef ref;
  setWidgetRef(ref) {
    this.ref = ref;
  }

  RxList<SingleWordModel> words = <SingleWordModel>[].obs;
  setWordList(RxInt setNumber) {
    WordSetModel wordSetModel = ref.watch(wordSetProvider.notifier).getWordSetBySetNumber(setNumber.value);
    words.value = wordSetModel.words;
    isLoading.value = false;
  }

  late List<int> setNumberList = getSetNumberList();
  String setNumberString = "Set  1";
  RxInt setNumber = 1.obs;

  List<DropdownMenuItem<String>> getSetNumberDropDownItem() {
    List<DropdownMenuItem<String>> dropdownItems = setNumberList.map(
      (int set) {
        return DropdownMenuItem<String>(
          value: set.toString(),
          child: Text('Set  $set'),
        );
      },
    ).toList();
    return dropdownItems;
  }

  void updateSelectedSetNumber(String selectedSet) {
    int selectedId = int.parse(selectedSet);
    for (int set in setNumberList) {
      if (set == selectedId) {
        setNumberString = set.toString();
        setNumber.value = selectedId;
        updateWordNumberAndSetNumber();
      }
    }
  }

  void updateWordNumberAndSetNumber() {
    wordNumber.value = 1;
    setWordList(setNumber);
  }

  RxInt wordNumber = 1.obs;
  goNextWord() {
    if (wordNumber < 5) {
      wordNumber = wordNumber + 1;
    } else {
      showSnackbar('Notify', 'No more word in current set');
    }
  }

  goPreviousWord() {
    if (wordNumber > 1) {
      wordNumber = wordNumber - 1;
    } else {
      showSnackbar('Notify', 'No more word in current set');
    }
  }

  @override
  void onInit() async {
    await syncAllDtaForThisPage();
    super.onInit();
  }

  getTodaysWord() {
    return SingleWordFullDataModel.defaultValue();
  }

  getWord(RxInt number) {
    SingleWordModel returnWord = SingleWordModel.defaultValue();
    for (SingleWordModel model in words) {
      if (model.id == (number.value - 1)) {
        returnWord = model;
      }
    }
    return returnWord;
  }

  Future<bool> syncAllDtaForThisPage() async {
    isLoading.value = false;
    return true;
  }

  List<int> getSetNumberList() {
    int length = ref.watch(wordSetProvider.notifier).getWordSetModelListLength();
    List<int> result = List.generate(length, (index) => (index + 1).toInt());
    return result;
  }
}
