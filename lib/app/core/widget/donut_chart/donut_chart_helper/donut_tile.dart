// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/target_achivement_donut_data_model.dart';
import 'package:voacl_lab/app/core/widget/data_card/white_data_card.dart';
import 'package:voacl_lab/app/core/widget/donut_chart/donut_chart_helper/donut_tile_container.dart';

class DonutTile extends StatelessWidget {
  TargetDonutStatModel targetAchievementCount;
  DonutTile({
    Key? key,
    required this.targetAchievementCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WhiteDataCard(
      child: DonutTileContainer(
        targetAchievementCount: targetAchievementCount,
      ),
    );
  }
}
