import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:voacl_lab/app/core/widget/data_card/white_data_card.dart';
import 'package:voacl_lab/app/modules/learn/controllers/learn_view_controller.dart';

class WordNavigationContainer extends ConsumerWidget {
  final LearnViewController controller;

  const WordNavigationContainer({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: WhiteDataCard(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: InkWell(
                onTap: () => {
                  controller.goPreviousWord(),
                },
                child: Container(
                  alignment: Alignment.topLeft,
                  child: Icon(
                    Icons.arrow_circle_left_outlined,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: InkWell(
                onTap: () => {
                  controller.goNextWord(),
                },
                child: Container(
                  alignment: Alignment.topRight,
                  child: Icon(
                    Icons.arrow_circle_right_outlined,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
