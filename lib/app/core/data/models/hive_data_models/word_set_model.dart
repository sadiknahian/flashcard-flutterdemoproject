import 'package:hive/hive.dart';
import 'package:voacl_lab/app/core/data/hive_table_constant.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/single_word_full_data_model.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/single_word_model.dart';
import 'package:voacl_lab/app/core/services/api_call_helper/http_communication.dart';

part 'word_set_model.g.dart';

@HiveType(typeId: HiveTableConstants.wordSetTypeId)
class WordSetModel extends HiveObject {
  @HiveField(0)
  int setId = 0;

  @HiveField(1, defaultValue: -1)
  int setNumber = -1;

  @HiveField(2, defaultValue: -1)
  int totalWordComplete = -1;

  @HiveField(3, defaultValue: false)
  bool isSetComplete = false;

  @HiveField(4)
  List<SingleWordModel> words = [];

  WordSetModel({
    required this.setId,
    required this.setNumber,
    required this.totalWordComplete,
    required this.isSetComplete,
    required this.words,
  });

  WordSetModel.fromJson(Map<String, dynamic> json) {
    setId = json['set_id'];
    setNumber = json['set'];
    totalWordComplete = json['word_complete'];
    isSetComplete = json['is_set_complete'];
    words = getWordList(json['words']);
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['set_id'] = setId;
    data['set'] = setNumber;
    data['word_complete'] = totalWordComplete;
    data['is_set_complete'] = isSetComplete;
    data['words'] = words;
    return data;
  }

  List<SingleWordModel> getWordList(jsonList) {
    List<SingleWordModel> wordList = [];
    for (JSON json in jsonList) {
      wordList.add(
        SingleWordModel.fromJson(json),
      );
    }
    return wordList;
  }
}
