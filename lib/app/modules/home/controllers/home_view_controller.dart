import 'package:get/get.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/single_person_score_model.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/target_achivement_donut_data_model.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/single_word_model.dart';
import 'package:voacl_lab/app/core/data_sync/donut_chart_data_sync_controller.dart';
import 'package:voacl_lab/app/core/data_sync/score_card_data_sync_controller.dart';
import 'package:voacl_lab/app/core/data_sync/word_set_data_sync_controller.dart';

class HomeViewController extends GetxController {
  RxBool isLoading = true.obs;
  late TargetDonutStatModel targetDonutStatModel =
      TargetDonutStatModel.defaultValue();
  late List<SinglePersonScore> scoreList = [];

  @override
  void onInit() async {
    await syncAllDtaForThisPage(); //have to call before super for data to arrange before GUI is created
    super.onInit();
  }

  getTodaysWord() {
    return SingleWordModel.defaultValue();
  }

  Future<bool> syncAllDtaForThisPage() async {
    setTargetDonutStatModel().then(
      (value) {
        setScoreBoardData().then(
          (value) {
            setWordSetData().then(
              (value) => {
                isLoading.value = false,
              },
            );
          },
        );
      },
    );
    return true;
  }

  Future<bool> setTargetDonutStatModel() async {
    targetDonutStatModel = await syncDonutChartData();
    return true;
  }

  Future<bool> setScoreBoardData() async {
    scoreList = await syncScoreCardData();
    return true;
  }

  Future<bool> setWordSetData() async {
    await syncWordSetData();
    return true;
  }
}
