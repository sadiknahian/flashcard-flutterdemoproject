import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:voacl_lab/app/core/widget/data_card/white_data_card.dart';
import 'package:voacl_lab/app/modules/home/views/home_view_helper/todays_word_card_container/todays_word_card/todays_word_card.dart';
import 'package:voacl_lab/app/modules/learn/controllers/learn_view_controller.dart';

class LearnViewWordContainer extends ConsumerWidget {
  final LearnViewController controller;
  const LearnViewWordContainer({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    controller.setWidgetRef(ref);
    controller.setWordList(controller.setNumber);
    return SizedBox(
      height: 300,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: WhiteDataCard(
          child: Obx(
            () => TodaysWordCard(
              showWordOfTheDayHeader: false,
              data: controller.getWord(controller.wordNumber),
            ),
          ),
        ),
      ),
    );
  }
}
