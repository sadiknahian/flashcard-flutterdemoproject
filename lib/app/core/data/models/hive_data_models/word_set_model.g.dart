// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'word_set_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class WordSetModelAdapter extends TypeAdapter<WordSetModel> {
  @override
  final int typeId = 2;

  @override
  WordSetModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return WordSetModel(
      setId: fields[0] as int,
      setNumber: fields[1] == null ? -1 : fields[1] as int,
      totalWordComplete: fields[2] == null ? -1 : fields[2] as int,
      isSetComplete: fields[3] == null ? false : fields[3] as bool,
      words: (fields[4] as List).cast<SingleWordModel>(),
    );
  }

  @override
  void write(BinaryWriter writer, WordSetModel obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.setId)
      ..writeByte(1)
      ..write(obj.setNumber)
      ..writeByte(2)
      ..write(obj.totalWordComplete)
      ..writeByte(3)
      ..write(obj.isSetComplete)
      ..writeByte(4)
      ..write(obj.words);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is WordSetModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
