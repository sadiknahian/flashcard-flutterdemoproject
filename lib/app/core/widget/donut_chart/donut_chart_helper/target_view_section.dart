import 'package:flutter/material.dart';
import 'package:voacl_lab/app/core/app_theme/app_theme.dart';
import 'package:voacl_lab/app/core/constant/string_constant/string_constant.dart';
import 'package:voacl_lab/app/core/constant/style/font_size.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/target_achivement_donut_data_model.dart';
import 'package:voacl_lab/app/core/widget/donut_chart/donut_cart_function/donut_cart_helper_functions.dart';
import 'package:voacl_lab/app/core/widget/donut_chart/donut_chart_helper/donut_chart.dart';

class TargetViewSection extends StatelessWidget {
  final List<DonutChartDataModel> chartData;
  final TargetDonutStatModel targetAchievementData;

  const TargetViewSection({
    Key? key,
    required this.chartData,
    required this.targetAchievementData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          flex: 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: getHeightAndWidth(context),
                height: getHeightAndWidth(context),
                child: DonutWidget(
                  chartData: chartData,
                  smallFont: true,
                ),
              ),
            ],
          ),
        ),
        Expanded(
          flex: 2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                ConstantStrings.donutChartTargetAchievementText,
                style: TextStyle(
                  color: AppColorSet.textPrimaryColor,
                  fontWeight: FontWeight.w500,
                  fontSize: FontSize.small,
                ),
                overflow: TextOverflow.visible,
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                "${(targetAchievementData.achievement).floor()} / ${(targetAchievementData.target).floor()}",
                style: const TextStyle(
                  color: AppColorSet.textTertiaryColor,
                  fontWeight: FontWeight.w800,
                  fontSize: FontSize.medium,
                ),
                overflow: TextOverflow.visible,
              ),
            ],
          ),
        )
      ],
    );
  }

  getHeightAndWidth(BuildContext context) {
    return MediaQuery.of(context).size.width * 0.21;
  }
}
