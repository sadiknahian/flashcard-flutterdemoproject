import 'package:flutter/material.dart';
import 'package:voacl_lab/app/core/app_theme/app_theme.dart';
import 'package:voacl_lab/app/core/constant/style/font_size.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/single_person_score_model.dart';

class ScoreRowGenerator extends StatelessWidget {
  final SinglePersonScore data;
  const ScoreRowGenerator({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 5,
        horizontal: 0,
      ),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.2, color: getColor(data.position)),
        ),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Text(
                data.name,
                style: TextStyle(
                  fontSize: FontSize.medium,
                  fontWeight: FontWeight.w400,
                  color: getColor(data.position),
                ),
                overflow: TextOverflow.visible,
              ),
            ],
          ),
          Column(
            children: [
              Text(
                data.score.toString(),
                style: TextStyle(
                  fontSize: FontSize.medium,
                  fontWeight: FontWeight.w400,
                  color: getColor(data.position),
                ),
                overflow: TextOverflow.visible,
              ),
            ],
          ),
        ],
      ),
    );
  }

  getColor(int position) {
    if (position % 2 == 0) {
      return AppColorSet.textTertiaryColor;
    } else {
      return AppColorSet.textSecondaryColor;
    }
  }
}
