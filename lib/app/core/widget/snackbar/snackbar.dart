import 'package:flutter/material.dart';
import 'package:get/get.dart';

SnackbarController showSnackbar(String title, String msg,
    [Icon? icon, Duration? duration]) {
  return Get.snackbar(
    title,
    msg,
    icon: icon,
    snackPosition: SnackPosition.BOTTOM,
    duration: duration ?? const Duration(seconds: 3),
    margin: const EdgeInsets.symmetric(vertical: 75, horizontal: 20),
  );
}
