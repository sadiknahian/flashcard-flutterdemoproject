class TargetDonutStatModel {
  int target = 0;
  int achievement = 0;
  int testCompleted = 0;
  double masterScore = 0;
  double lastTestScore = 0;

  TargetDonutStatModel({
    required this.target,
    required this.achievement,
    required this.testCompleted,
    required this.masterScore,
    required this.lastTestScore,
  });

  TargetDonutStatModel.defaultValue() {
    target = 0;
    achievement = 0;
    testCompleted = 0;
    masterScore = 0;
    lastTestScore = 0;
  }
}
