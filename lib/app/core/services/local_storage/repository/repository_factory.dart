import 'package:voacl_lab/app/core/data/hive_table_constant.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/score_board_model.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/single_word_model.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/user.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/word_set_model.dart';
import 'package:voacl_lab/app/core/services/local_storage/repository/repository.dart';
import 'package:voacl_lab/app/core/services/local_storage/repository/repository_impl.dart';

class RepoFactory {
  static Repository<User> get user =>
      BaseRepositoryImpl<User>(tableName: HiveTableConstants.userTableName);

  static Repository<SinglePersonScoreModel> get singlePersonScoreModel =>
      BaseRepositoryImpl<SinglePersonScoreModel>(
          tableName: HiveTableConstants.scoreBoardTableName);

  static Repository<WordSetModel> get wordSetModel =>
      BaseRepositoryImpl<WordSetModel>(
          tableName: HiveTableConstants.wordSetTableName);

  static Repository<SingleWordModel> get singleWordModel =>
      BaseRepositoryImpl<SingleWordModel>(
          tableName: HiveTableConstants.singleWordModelTableName);
}
