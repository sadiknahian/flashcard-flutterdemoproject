// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'single_word_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SingleWordModelAdapter extends TypeAdapter<SingleWordModel> {
  @override
  final int typeId = 3;

  @override
  SingleWordModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SingleWordModel(
      id: fields[0] as int,
      word: fields[1] == null ? '' : fields[1] as String,
      meaning: fields[2] == null ? '' : fields[2] as String,
      example: fields[3] == null ? '' : fields[3] as String,
    );
  }

  @override
  void write(BinaryWriter writer, SingleWordModel obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.word)
      ..writeByte(2)
      ..write(obj.meaning)
      ..writeByte(3)
      ..write(obj.example);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SingleWordModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
