class DonutChartDataModel {
  DonutChartDataModel(this.x, this.y);
  final String x;
  final double y;
}

double getAchievementPercentage(int target, int achievement) {
  if (target == 0) {
    return 0;
  }
  return (achievement / target) * 100;
}

double getTargetPercentage(int target, int achievement) {
  double achievementPercentage = getAchievementPercentage(target, achievement);
  if (achievementPercentage >= 100) {
    return 0;
  }
  return 100 - achievementPercentage;
}
