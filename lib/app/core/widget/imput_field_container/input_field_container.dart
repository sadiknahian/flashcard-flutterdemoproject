import 'package:flutter/material.dart';

class InputFieldContainer extends StatelessWidget {
  final Widget child;
  const InputFieldContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 2),
      child: SizedBox(
        child: child,
      ),
    );
  }
}
