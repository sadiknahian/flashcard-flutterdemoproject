abstract class HiveTableConstants {
  static const int userTypeId = 0;
  static const String userTableName = 'user';

  static const int scoreBoardTypeId = 1;
  static const String scoreBoardTableName = 'scoreBoard';

  static const int wordSetTypeId = 2;
  static const String wordSetTableName = 'wordSet';

  static const int singleWordModelTypeId = 3;
  static const String singleWordModelTableName = 'singleWord';
}
