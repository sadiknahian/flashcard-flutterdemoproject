import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:voacl_lab/app/core/services/api_call_helper/failure.dart';

typedef JSON = Map<String, dynamic>;
typedef FailureOrJSON = Either<Failure, JSON>;

class HttpClient {
  final String url;
  dynamic data;

  HttpClient({required this.url, this.data});

  final dio = Dio();

  JSON convertPayloadToJson(dynamic data) {
    if (data is List<dynamic>) {
      return {'data': data};
    } else if (data is int) {
      return {'data': data};
    }
    return data;
  }

  JSON convertToJson(dynamic data) {
    return {'data': data};
  }

  Future<FailureOrJSON> post() async {
    dynamic response;
    try {
      response = await dio.post(
        url,
        data: data,
      );
      if (response.statusCode == 200 && response.data['result'] != null) {
        if (response.data['result']['success']) {
          JSON convertedJson =
              convertPayloadToJson(response.data['result']['data']);
          convertedJson['message'] = response.data['result']['message'];
          return Right(convertedJson);
        } else {
          return Left(NoInternet(message: response.data['result']['message']));
        }
      } else if (response.data['error'] != null) {
        return Left(
            NoInternet(message: response.data['error']['data']['message']));
      }
      return Left(NoInternet(message: 'Server Error'));
    } on DioError catch (err) {
      if (err.response?.statusCode != null) {
        return Left(
          NoInternet(message: '$err.message'),
        );
      }
      return Left(
        NoInternet(message: "Please Check Internet Connection"),
      );
    } on Exception catch (err) {
      return Left(
        NoInternet(message: '$err'),
      );
    } on Error catch (err) {
      return Left(
        NoInternet(message: '$err'),
      );
    }
  }

  Future<FailureOrJSON> get() async {
    dynamic response;
    try {
      response = await dio.get(url);
      return Right(convertToJson(response));
    } on DioError catch (err) {
      if (err.response?.statusCode != null) {
        return Left(
          NoInternet(message: '$err.message'),
        );
      }
      return Left(
        NoInternet(message: "Please Check Internet Connection"),
      );
    } on Exception catch (err) {
      return Left(
        NoInternet(message: '$err'),
      );
    } on Error catch (err) {
      return Left(
        NoInternet(message: '$err'),
      );
    }
  }
}
