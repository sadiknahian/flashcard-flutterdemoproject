class SinglePersonScore {
  String name = '';
  double score = 0;
  int position = -1;

  SinglePersonScore({
    required this.name,
    required this.score,
    required this.position,
  });

  SinglePersonScore.defaultValue() {
    name = 'No Name';
    score = 0;
    position = 1;
  }
}
