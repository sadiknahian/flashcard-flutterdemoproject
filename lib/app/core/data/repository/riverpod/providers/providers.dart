import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/word_set_model.dart';
import 'package:voacl_lab/app/core/data/repository/riverpod/notifiers/word_set_notifier.dart';
import 'package:voacl_lab/app/core/services/local_storage/repository/repository_factory.dart';

final wordSetProvider =
    StateNotifierProvider.autoDispose<WordSetNotifier, List<WordSetModel>>(
  (ref) {
    return WordSetNotifier(RepoFactory.wordSetModel.readAll());
  },
);
