// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:voacl_lab/app/core/app_theme/app_theme.dart';
import 'package:voacl_lab/app/core/constant/style/font_size.dart';
import 'package:voacl_lab/app/core/widget/donut_chart/donut_cart_function/donut_cart_helper_functions.dart';

class DonutWidget extends StatelessWidget {
  List<DonutChartDataModel>? chartData;
  final bool smallFont;

  DonutWidget({Key? key, this.chartData, this.smallFont = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SfCircularChart(
      margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      palette: const [
        AppColorSet.secondary,
        AppColorSet.primary,
      ],
      annotations: <CircularChartAnnotation>[
        CircularChartAnnotation(
          widget: Text(
            "${chartData?.first.y.toInt()}%",
            style: smallFont ? const TextStyle(fontSize: FontSize.medium) : const TextStyle(fontSize: FontSize.large),
          ),
        ),
      ],
      series: <CircularSeries>[
        DoughnutSeries<DonutChartDataModel, String>(
          dataSource: chartData,
          xValueMapper: (DonutChartDataModel data, _) => data.x,
          yValueMapper: (DonutChartDataModel data, _) => data.y,
          innerRadius: '80%',
        )
      ],
    );
  }
}
