import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:voacl_lab/app/core/app_theme/app_theme.dart';
import 'package:voacl_lab/app/core/constant/string_constant/string_constant.dart';
import 'package:voacl_lab/app/core/constant/style/font_size.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/target_achivement_donut_data_model.dart';

class InfoViewSection extends ConsumerWidget {
  final TargetDonutStatModel targetAchievementData;

  const InfoViewSection({
    Key? key,
    required this.targetAchievementData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SolitaryDataViewer(
          title: ConstantStrings.completedOnlineTestText,
          textMsg: targetAchievementData.testCompleted.toString(),
        ),
        SolitaryDataViewer(
          title: ConstantStrings.overAllScoreText,
          textMsg: targetAchievementData.masterScore.toString(),
        ),
        SolitaryDataViewer(
          title: ConstantStrings.lastExamScoreText,
          textMsg: targetAchievementData.lastTestScore.toString(),
        ),
      ],
    );
  }
}

class SolitaryDataViewer extends StatelessWidget {
  final String title;
  final String textMsg;

  const SolitaryDataViewer({
    Key? key,
    required this.title,
    required this.textMsg,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: const TextStyle(
              color: AppColorSet.textPrimaryColor,
              fontWeight: FontWeight.w500,
              fontSize: FontSize.medium,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 2),
          Text(
            textMsg,
            style: const TextStyle(
              color: AppColorSet.textTertiaryColor,
              fontWeight: FontWeight.w800,
              fontSize: FontSize.medium,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
