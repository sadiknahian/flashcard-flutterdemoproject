import 'package:dartz/dartz.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/word_set_model.dart';
import 'package:voacl_lab/app/core/services/api_call_helper/http_communication.dart';
import 'package:voacl_lab/app/core/services/local_storage/repository/repository_factory.dart';

Future<bool> syncWordSetData() async {
  JSON data = getDummyJsonData();
  FailureOrJSON response = Right(data);
  return parseWordSetData(response);
}

bool parseWordSetData(FailureOrJSON failureOrJson) {
  return failureOrJson.fold(
    (failure) {
      return false;
    },
    (json) {
      return saveWordSetData(json);
    },
  );
}

bool saveWordSetData(JSON json) {
  RepoFactory.wordSetModel.deleteAll();
  for (JSON json in json['set_list']) {
    WordSetModel model = WordSetModel.fromJson(json);
    print(model.words.length);
    RepoFactory.wordSetModel.create(model.setId, model);
  }
  return true;
}

JSON getDummyJsonData() {
  JSON data = {
    'set_list': [
      {
        "set_id": 101,
        "set": 1,
        "word_complete": 0,
        "is_set_complete": false,
        "words": [
          {
            "id": 0,
            "word": "Abide",
            "meaning": "Accept or act in accordance with",
            "example": "I said I would abide by their decision."
          },
          {
            "id": 1,
            "word": "Amalgamate",
            "meaning": "Combine or unite to form one organization or structure",
            "example": "He amalgamated his company with another."
          },
          {
            "id": 2,
            "word": "Allocation",
            "meaning":
                "He action or process of allocating or sharing out something",
            "example": "More efficient allocation of resources"
          },
          {
            "id": 3,
            "word": "Action",
            "meaning":
                "The fact or process of doing something, typically to achieve an aim",
            "example": "Ending child labour will require action on many levels"
          },
          {
            "id": 4,
            "word": "Await",
            "meaning": "Wait for (an event).",
            "example": "We await the proposals with impatience"
          }
        ]
      },
      {
        "set_id": 102,
        "set": 2,
        "word_complete": 0,
        "is_set_complete": false,
        "words": [
          {
            "id": 0,
            "word": "Barometer",
            "meaning":
                "Something that is used to indicate or predict something",
            "example":
                "The test is used as a barometer to measure a student's reading level."
          },
          {
            "id": 1,
            "word": "Baggage",
            "meaning":
                "The feelings, beliefs, problems, or past events that can make life difficult for a person or group",
            "example": "The emotional baggage I'm hauling around"
          },
          {
            "id": 2,
            "word": "Begin",
            "meaning":
                "Perform or undergo the first part of (an action or activity)",
            "example": "Peter had just begun a life sentence for murder"
          },
          {
            "id": 3,
            "word": "Bewitch",
            "meaning": "Enchant and delight (someone)",
            "example":
                "They both were bewitched by the golden luminosity of Italy"
          },
          {
            "id": 4,
            "word": "Beloved",
            "meaning": "Dearly loved",
            "example": "They were forced to leave their beloved homeland"
          }
        ]
      }
    ]
  };
  return data;
}
