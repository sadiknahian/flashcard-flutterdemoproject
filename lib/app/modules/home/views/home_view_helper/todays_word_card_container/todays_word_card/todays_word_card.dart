import 'package:flutter/material.dart';
import 'package:voacl_lab/app/core/app_theme/app_theme.dart';
import 'package:voacl_lab/app/core/constant/string_constant/string_constant.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/single_word_model.dart';

import 'solitary_data_viewer/solitary_data-viewer.dart';

class TodaysWordCard extends StatelessWidget {
  final bool showWordOfTheDayHeader;
  final SingleWordModel data;
  const TodaysWordCard({
    Key? key,
    this.showWordOfTheDayHeader = true,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        showWordOfTheDayHeader
            ? SolitaryDataViewer(
                title: ConstantStrings.wordOfTheDayText,
                isHeader: true,
              )
            : Container(),
        SizedBox(height: 16),
        SolitaryDataViewer(
          title: data.word,
          isHeader: true,
          textColor: AppColorSet.textTertiaryColor,
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 16),
        SolitaryDataViewer(
          title: ConstantStrings.wordOfTheDayMeaningText,
          textColor: AppColorSet.textSecondaryColor,
        ),
        SolitaryDataViewer(title: data.meaning),
        SizedBox(height: 8),
        SolitaryDataViewer(
          title: ConstantStrings.wordOfTheDayExampleText,
          textColor: AppColorSet.textSecondaryColor,
        ),
        SolitaryDataViewer(title: data.example),
      ],
    );
  }
}
