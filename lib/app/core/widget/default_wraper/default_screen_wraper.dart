import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'default_wraper_helper/app_bar_title.dart';
import 'default_wraper_helper/menu_drawer.dart';

class DefaultScreenWraper extends StatelessWidget {
  final String title;
  final Widget child;
  final bool? centerTitle;
  final bool showTopBar;
  final bool showBackBtn;
  final bool showTopBarMenu;
  final bool isScrollable;
  final Color? topBarColor;
  final double? elevation;
  final TextStyle? titleTextStyle;
  final VoidCallback? onBackPressed;
  final Widget? icon;
  final EdgeInsets? margin;

  const DefaultScreenWraper({
    Key? key,
    required this.child,
    this.title = ' ',
    this.centerTitle = true,
    this.showTopBar = true,
    this.showBackBtn = true,
    this.showTopBarMenu = true,
    this.isScrollable = true,
    this.topBarColor,
    this.titleTextStyle,
    this.elevation,
    this.onBackPressed,
    this.margin = const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Banner(
      message: getBannerText(),
      textStyle: const TextStyle(
        color: Colors.black,
        fontSize: 13,
        fontWeight: FontWeight.bold,
      ),
      location: BannerLocation.topStart,
      color: Colors.yellow,
      child: getScreenComponents(),
    );
  }

  Scaffold getScreenComponents() {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
          leading: showBackBtn
              ? IconButton(
                  icon: const Icon(Icons.arrow_back, color: Colors.white),
                  onPressed: () {
                    onBackPressed != null ? onBackPressed!() : Get.back();
                  },
                )
              : null,
          title: AppBarTitle(
            icon: icon,
            title: title,
          ),
          centerTitle: centerTitle,
          titleTextStyle: titleTextStyle,
          backgroundColor: topBarColor,
          elevation: elevation),
      body: isScrollable
          ? SingleChildScrollView(
              child: Container(
                margin: margin,
                child: child,
              ),
            )
          : Container(margin: margin, child: child),
      endDrawer: showTopBarMenu ? MenuDrawer() : null,
    );
  }
}

String getBannerText() {
  String bannerText = 'DEMO';
  return bannerText;
}
