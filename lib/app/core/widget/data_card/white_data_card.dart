import 'package:flutter/material.dart';

class WhiteDataCard extends StatelessWidget {
  final VoidCallback? callbackFunction;
  final Widget child;
  final EdgeInsets padding;

  const WhiteDataCard({
    Key? key,
    required this.child,
    this.callbackFunction,
    this.padding = const EdgeInsets.all(10),
  }) : super(key: key);

  final double boxShadowOffset = 1;
  final double boxShadowBlurRadius = 3;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 1.3,
      borderRadius: BorderRadius.circular(10),
      child: Container(
        padding: padding,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        child: InkWell(
          onTap: () {
            if (callbackFunction != null) callbackFunction!();
          },
          child: Center(
            child: child,
          ),
        ),
      ),
    );
  }
}
