import 'package:flutter/material.dart';

class LoadingAnimation extends StatelessWidget {
  final double height;
  final double? width;

  const LoadingAnimation({
    Key? key,
    this.height = 200,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    var padding = MediaQuery.of(context).viewPadding;
    double actualScreenHeight = screenHeight - padding.top - kToolbarHeight;

    return SizedBox(
      height: height,
      width: width ?? width,
      child: const Center(child: CircularProgressIndicator()),
    );
  }
}
