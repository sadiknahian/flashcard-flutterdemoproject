import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/single_word_model.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/word_set_model.dart';

class WordSetNotifier extends StateNotifier<List<WordSetModel>> {
  List<WordSetModel> originalState = [];

  WordSetNotifier(state) : super(state) {
    originalState = state;
  }

  List<SingleWordModel> getSingleWordFullDataModel() {
    return state.first.words;
  }

  WordSetModel getWordSetBySetNumber(int setNumber) {
    WordSetModel returnModel = state.first;
    for (WordSetModel model in state) {
      if (model.setNumber == setNumber) {
        returnModel = model;
      }
    }
    return returnModel;
  }

  getWordSetModelListLength() {
    return state.length;
  }

  void reset(int routeId) {
    state = [];
  }
}
