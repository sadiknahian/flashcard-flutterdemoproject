import 'package:intl/intl.dart';

String formatFinancialNumbers(dynamic number, [bool? isFractionEnabled]) {
  double mainValue;
  bool fraction = isFractionEnabled ?? false;
  String returnValue = "";
  // final numberFormat = NumberFormat("#,##,##0.00", "en_US");
  final numberFormat = fraction
      ? NumberFormat("#,##,##0.00", "en_US")
      : NumberFormat("#,##,##0", "en_US");

  if (number == null) {
    returnValue = "null";
    return returnValue;
  }

  if (number is String) {
    mainValue = double.tryParse(number) ?? 0;
  } else if (number is int) {
    mainValue = number.toDouble();
  } else if (number is double) {
    mainValue = number;
  } else {
    returnValue = "invalid";
    return returnValue;
  }

  returnValue = numberFormat.format(mainValue);
  return returnValue;
}
