import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:voacl_lab/app/core/widget/data_card/white_data_card.dart';
import 'package:voacl_lab/app/modules/home/controllers/home_view_controller.dart';
import 'package:voacl_lab/app/modules/home/views/home_view_helper/todays_word_card_container/todays_word_card/todays_word_card.dart';

class TodaysWordCardContainer extends ConsumerWidget {
  final HomeViewController controller;
  const TodaysWordCardContainer({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SizedBox(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: WhiteDataCard(
          child: TodaysWordCard(
            data: controller.getTodaysWord(),
          ),
        ),
      ),
    );
  }
}
