import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/target_achivement_donut_data_model.dart';

import 'donut_chart_helper/donut_tile.dart';

class DonutChartConsumer extends ConsumerWidget {
  final TargetDonutStatModel targetAchievementCount;

  const DonutChartConsumer({
    Key? key,
    required this.targetAchievementCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return DonutTile(
      targetAchievementCount: targetAchievementCount,
    );
  }
}
