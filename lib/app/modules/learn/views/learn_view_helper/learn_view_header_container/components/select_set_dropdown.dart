import 'package:flutter/material.dart';
import 'package:voacl_lab/app/core/widget/custom_dropdown/custom_dropdown.dart';
import 'package:voacl_lab/app/core/widget/imput_field_container/input_field_container.dart';
import 'package:voacl_lab/app/modules/learn/controllers/learn_view_controller.dart';

class SelectSetDropdown extends StatelessWidget {
  final LearnViewController controller;
  const SelectSetDropdown({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InputFieldContainer(
      child: CustomDropdown(
        hintText: controller.setNumberString,
        dropdownItems: controller.getSetNumberDropDownItem(),
        dropdownValueSetter: controller.updateSelectedSetNumber,
        dropdownValueGetter: () => controller.setNumber.value,
      ),
    );
  }
}
