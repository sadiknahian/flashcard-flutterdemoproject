import 'package:flutter/material.dart';

class AppBarTitle extends StatelessWidget {
  final Widget? icon;
  final String title;

  const AppBarTitle({
    Key? key,
    this.icon,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (icon == null) {
      return Text(
        title,
        overflow: TextOverflow.visible,
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title,
            overflow: TextOverflow.ellipsis,
          ),
          const VerticalDivider(),
          icon!,
        ],
      );
    }
  }
}
