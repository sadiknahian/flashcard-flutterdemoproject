import 'package:flutter/material.dart';

ThemeData appThemeData() {
  return ThemeData(
    colorScheme: ColorScheme.fromSwatch().copyWith(
      primary: Color.fromARGB(255, 115, 227, 242),
      secondary: Colors.amber,
    ),
  );
}

abstract class AppColorSet {
  AppColorSet._();

  static const primary = Color.fromARGB(255, 115, 227, 242);
  static const secondary = Colors.amber;
  static const textPrimaryColor = Color.fromARGB(210, 0, 0, 0);
  static const textSecondaryColor = Color.fromARGB(255, 6, 190, 207);
  static const textTertiaryColor = Colors.amber;
}
