import 'package:flutter/material.dart';

class Menu {
  Menu({
    required this.title,
    this.icon = Icons.verified_user,
    this.page,
    this.onPressed,
  });

  final String title;
  final IconData? icon;
  final String? page;
  VoidCallback? onPressed;
}
