abstract class FontSize {
  FontSize._();
  static const small = 10.0;
  static const medium = 12.0;
  static const large = 14.0;
  static const extraLarge = 16.0;

  static const header = 18.0;
}
