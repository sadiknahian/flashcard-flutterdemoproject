import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'app/core/app_theme/app_theme.dart';
import 'app/core/init/init_project.dart';
import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  await initAppDependencies();

  runApp(
    ProviderScope(
      child: GetMaterialApp(
        theme: appThemeData(),
        debugShowCheckedModeBanner: false,
        title: "Application",
        initialRoute: Routes.HOME,
        getPages: AppPages.routes,
      ),
    ),
  );
}
