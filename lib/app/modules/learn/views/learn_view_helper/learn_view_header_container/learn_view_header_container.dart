import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:voacl_lab/app/core/widget/data_card/white_data_card.dart';
import 'package:voacl_lab/app/modules/learn/controllers/learn_view_controller.dart';
import 'package:voacl_lab/app/modules/learn/views/learn_view_helper/learn_view_header_container/components/set_info_section.dart';

import 'components/select_set_dropdown.dart';

class LearnViewHeaderContainer extends ConsumerWidget {
  final LearnViewController controller;

  const LearnViewHeaderContainer({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    controller.setWidgetRef(ref);
    controller.setWordList(controller.setNumber);
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 4),
      child: WhiteDataCard(
        padding: EdgeInsets.fromLTRB(6, 6, 6, 6),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 1,
              child: SelectSetDropdown(
                controller: controller,
              ),
            ),
            Expanded(
              flex: 1,
              child: SetInfoSection(
                controller: controller,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
