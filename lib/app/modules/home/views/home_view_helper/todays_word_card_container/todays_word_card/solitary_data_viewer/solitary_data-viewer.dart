import 'package:flutter/material.dart';
import 'package:voacl_lab/app/core/app_theme/app_theme.dart';
import 'package:voacl_lab/app/core/constant/style/font_size.dart';

class SolitaryDataViewer extends StatelessWidget {
  final String title;
  final bool isHeader;
  final Color textColor;
  final TextAlign textAlign;

  const SolitaryDataViewer({
    Key? key,
    required this.title,
    this.isHeader = false,
    this.textColor = AppColorSet.textPrimaryColor,
    this.textAlign = TextAlign.start,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 8, 0, 2),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              title,
              style: TextStyle(
                color: textColor,
                fontWeight: isHeader ? FontWeight.w800 : FontWeight.normal,
                fontSize: isHeader ? FontSize.extraLarge : FontSize.medium,
              ),
              textAlign: textAlign,
              overflow: TextOverflow.visible,
            ),
          ),
          const SizedBox(height: 2),
        ],
      ),
    );
  }
}
