import 'package:flutter/material.dart';
import 'package:voacl_lab/app/core/app_theme/app_theme.dart';
import 'package:voacl_lab/app/core/widget/menu/menu_generator.dart';

class MenuDrawer extends StatelessWidget {
  MenuDrawer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.amber,
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(
              'Sadik',
              style: TextStyle(color: AppColorSet.textPrimaryColor),
            ),
            accountEmail: Text(
              'sadik.nahian@gmail.com',
              style: TextStyle(color: AppColorSet.textPrimaryColor),
            ),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.amber,
              child: Text(
                getFirstCharacterOfName('Sadik'),
                style: const TextStyle(fontSize: 40.0),
              ),
            ),
          ),
          ...MenuGenerator.getDrawerMenus().map((e) => e),
        ],
      ),
    );
  }
}

String getFirstCharacterOfName(String user) {
  return user.isNotEmpty ? user[0] : '';
}
