import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voacl_lab/app/core/constant/title_name_constant/title_name_constant.dart';
import 'package:voacl_lab/app/core/widget/default_wraper/default_screen_wraper.dart';
import 'package:voacl_lab/app/core/widget/donut_chart/donut_chart_consumer.dart';
import 'package:voacl_lab/app/core/widget/loading_animation/loading_animation.dart';
import 'package:voacl_lab/app/modules/home/controllers/home_view_controller.dart';
import 'package:voacl_lab/app/modules/home/views/home_view_helper/score_board_container/score_board_container.dart';

import 'home_view_helper/todays_word_card_container/todays_world_card_container.dart';

class HomeView extends GetView<HomeViewController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return DefaultScreenWraper(
      showBackBtn: false,
      title: TitleName.HOME,
      child: Obx(
        () => controller.isLoading.value
            ? LoadingAnimation()
            : Column(
                children: [
                  DonutChartConsumer(
                    targetAchievementCount: controller.targetDonutStatModel,
                  ),
                  TodaysWordCardContainer(
                    controller: controller,
                  ),
                  ScoreBoardContainer(
                    controller: controller,
                  ),
                ],
              ),
      ),
    );
  }
}
