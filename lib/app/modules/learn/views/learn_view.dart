import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voacl_lab/app/core/constant/title_name_constant/title_name_constant.dart';
import 'package:voacl_lab/app/core/widget/default_wraper/default_screen_wraper.dart';
import 'package:voacl_lab/app/core/widget/loading_animation/loading_animation.dart';
import 'package:voacl_lab/app/modules/learn/controllers/learn_view_controller.dart';
import 'package:voacl_lab/app/modules/learn/views/learn_view_helper/learn_view_header_container/learn_view_header_container.dart';

import 'learn_view_helper/learn_view_word_container/learn_view_word_container.dart';
import 'learn_view_helper/word_navigation_container/word_navigation_container.dart';

class LearnView extends GetView<LearnViewController> {
  const LearnView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return DefaultScreenWraper(
      showBackBtn: false,
      title: TitleName.LEARN,
      child: Obx(
        () => controller.isLoading.value
            ? LoadingAnimation()
            : Column(
                children: [
                  LearnViewHeaderContainer(controller: controller),
                  LearnViewWordContainer(
                    controller: controller,
                  ),
                  WordNavigationContainer(controller: controller),
                ],
              ),
      ),
    );
  }
}
