import 'package:flutter/material.dart';
import 'package:voacl_lab/app/core/services/local_storage/init_storage.dart';

Future<void> initAppDependencies() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initStorage();
}
