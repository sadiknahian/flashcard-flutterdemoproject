import 'package:hive/hive.dart';
import 'package:voacl_lab/app/core/data/hive_table_constant.dart';

part 'single_word_model.g.dart';

@HiveType(typeId: HiveTableConstants.singleWordModelTypeId)
class SingleWordModel extends HiveObject {
  @HiveField(0)
  int id = 0;

  @HiveField(1, defaultValue: '')
  String word = '';

  @HiveField(2, defaultValue: '')
  String meaning = '';

  @HiveField(3, defaultValue: '')
  String example = '';

  SingleWordModel({
    required this.id,
    required this.word,
    required this.meaning,
    required this.example,
  });

  SingleWordModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    word = json['word'];
    meaning = json['meaning'];
    example = json['example'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['word'] = word;
    data['meaning'] = meaning;
    data['example'] = example;
    return data;
  }

  SingleWordModel.defaultValue() {
    id = 1;
    word = 'Information';
    meaning = 'Facts provided or learned about something or someone.';
    example = 'A vital piece of information.';
  }
}
