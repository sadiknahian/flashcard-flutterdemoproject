import 'package:flutter/material.dart';
import 'package:voacl_lab/app/routes/app_pages.dart';

import 'menu.dart';

Map<String, Menu> mainMenuMap = {
  "home": Menu(
    title: "Dashboard\n",
    icon: Icons.dashboard,
    page: Routes.HOME,
  ),
  "flashCard": Menu(
    title: "Flash Card\n",
    icon: Icons.credit_card_sharp,
    page: Routes.LEARN,
  ),
  "profile": Menu(
    title: "Profile\n",
    icon: Icons.account_box,
    page: Routes.HOME,
  ),
  "logout": Menu(
    title: "Logout\n",
    icon: Icons.logout,
    page: Routes.HOME,
  ),
};

enum MenuCode {
  home,
  flashCard,
  profile,
  logout,
}
