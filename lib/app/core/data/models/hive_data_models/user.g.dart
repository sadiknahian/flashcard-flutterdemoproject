// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserAdapter extends TypeAdapter<User> {
  @override
  final int typeId = 0;

  @override
  User read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return User(
      userID: fields[1] == null ? -1 : fields[1] as int?,
      userName: fields[2] == null ? '' : fields[2] as String,
      userMail: fields[3] == null ? '' : fields[3] as String,
      lastTestScore: fields[4] == null ? -1 : fields[4] as double,
      overAllTestScore: fields[5] == null ? -1 : fields[5] as double,
      totalTestTaken: fields[6] == null ? -1 : fields[6] as int,
      isPaidUser: fields[7] == null ? false : fields[7] as bool,
      lastPayDate: fields[8] == null ? '' : fields[8] as String,
      isDummyUser: fields[9] == null ? true : fields[9] as bool,
    )..id = fields[0] as int;
  }

  @override
  void write(BinaryWriter writer, User obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.userID)
      ..writeByte(2)
      ..write(obj.userName)
      ..writeByte(3)
      ..write(obj.userMail)
      ..writeByte(4)
      ..write(obj.lastTestScore)
      ..writeByte(5)
      ..write(obj.overAllTestScore)
      ..writeByte(6)
      ..write(obj.totalTestTaken)
      ..writeByte(7)
      ..write(obj.isPaidUser)
      ..writeByte(8)
      ..write(obj.lastPayDate)
      ..writeByte(9)
      ..write(obj.isDummyUser);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
