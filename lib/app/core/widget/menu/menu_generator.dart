import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voacl_lab/app/core/app_theme/app_theme.dart';

import 'menu.dart';
import 'menu_declaration.dart';

abstract class MenuGenerator {
  static List<Menu> getMainMenus() {
    List<Menu> getTheseMenus(List<MenuCode> menusToPick) {
      List<Menu> menusToExport = [];
      for (MenuCode menuCode in menusToPick) {
        if (mainMenuMap[menuCode.name] != null) {
          menusToExport.add(mainMenuMap[menuCode.name]!);
        }
      }
      return menusToExport;
    }

    return getTheseMenus([
      MenuCode.home,
      MenuCode.flashCard,
      MenuCode.profile,
      MenuCode.logout,
    ]);
  }

  static List<Widget> getDrawerMenus() {
    List<Widget> drawerMenus = [];
    for (Menu element in getMainMenus()) {
      drawerMenus.add(
        ListTile(
          leading: Icon(
            element.icon,
            color: AppColorSet.textPrimaryColor,
            size: 24,
          ),
          title: Padding(
            padding: const EdgeInsets.fromLTRB(12, 13, 0, 0),
            child: Text(
              element.title,
              style: TextStyle(
                  color: AppColorSet.textPrimaryColor,
                  fontSize: 18,
                  fontWeight: FontWeight.w500),
            ),
          ),
          onTap: () {
            if (element.onPressed != null) element.onPressed!();
            if (element.page != null) Get.offNamed(element.page!);
          },
        ),
      );
    }
    return drawerMenus;
  }
}
