import 'package:dartz/dartz.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/single_person_score_model.dart';
import 'package:voacl_lab/app/core/services/api_call_helper/http_communication.dart';

Future<List<SinglePersonScore>> syncScoreCardData() async {
  JSON data = getDummyJsonData();
  FailureOrJSON response = Right(data);
  return parseScoreCardData(response);
}

List<SinglePersonScore> parseScoreCardData(FailureOrJSON failureOrJson) {
  List<SinglePersonScore> singlePersonScore = [];
  singlePersonScore.add(SinglePersonScore.defaultValue());
  return failureOrJson.fold(
    (failure) {
      return singlePersonScore;
    },
    (json) {
      return saveData(json);
    },
  );
}

List<SinglePersonScore> saveData(JSON json) {
  List<SinglePersonScore> singlePersonScoreList = [];
  for (JSON json in json['score_board_data']) {
    singlePersonScoreList.add(
      SinglePersonScore(
        name: json['name'],
        score: json['score'] * 1.0,
        position: json['position'],
      ),
    );
  }
  return singlePersonScoreList;
}

JSON getDummyJsonData() {
  JSON data = {
    'score_board_data': [
      {"id": 1, "score": 25, "position": 1, "name": "Piper Gwillim"},
      {"id": 2, "score": 11, "position": 2, "name": "Mona Boswell"},
      {"id": 3, "score": 94, "position": 3, "name": "Ernesto Rogeon"},
      {"id": 4, "score": 43, "position": 4, "name": "Nicola Whitsey"},
      {"id": 5, "score": 91, "position": 5, "name": "Seth West-Frimley"},
      {"id": 6, "score": 10, "position": 6, "name": "Eugenius Lavall"},
      {"id": 7, "score": 48, "position": 7, "name": "Angelia Ledger"},
      {"id": 8, "score": 96, "position": 8, "name": "Mona Mitchely"},
      {"id": 9, "score": 22, "position": 9, "name": "Gayel Kopman"},
      {"id": 10, "score": 30, "position": 10, "name": "Abran Craiker"},
    ]
  };

  return data;
}
