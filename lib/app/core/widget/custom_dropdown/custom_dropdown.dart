import 'package:flutter/material.dart';

class CustomDropdown extends StatefulWidget {
  final List<DropdownMenuItem<String>> dropdownItems;
  final String? currentItem;
  final String titleText;
  final String hintText;
  final TextStyle hintTextStyle;
  final Function? dropdownValueGetter;
  final Function? dropdownValueSetter;
  final bool enabled;

  // final VoidCallback? dropdownValueSetter(String str);

  CustomDropdown({
    Key? key,
    required this.dropdownItems,
    this.currentItem,
    this.titleText = '',
    this.hintText = '',
    this.hintTextStyle = const TextStyle(fontSize: 14),
    this.dropdownValueGetter,
    this.dropdownValueSetter,
    this.enabled = true,
  }) : super(key: key);

  @override
  State<CustomDropdown> createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  // String dropdownValue = "";

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<String>(
      decoration: const InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
      ),
      itemHeight: 50,
      isDense: true,
      iconSize: 20,
      icon: const Icon(Icons.arrow_drop_down),
      elevation: 16,
      style: const TextStyle(color: Colors.black),
      hint: Text(
        widget.hintText,
        style: widget.hintTextStyle,
      ),
      onChanged: (String? newValue) {
        setState(
          () {
            widget.dropdownValueSetter!(newValue!);
          },
        );
        () => widget.dropdownValueSetter!(newValue!);
      },
      items: widget.enabled ? widget.dropdownItems : [],
    );
  }
}
