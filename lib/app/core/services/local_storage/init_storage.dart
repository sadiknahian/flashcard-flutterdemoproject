import 'package:hive_flutter/hive_flutter.dart';
import 'package:voacl_lab/app/core/data/hive_table_constant.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/score_board_model.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/single_word_model.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/user.dart';
import 'package:voacl_lab/app/core/data/models/hive_data_models/word_set_model.dart';

Future<void> initStorage() async {
  await Hive.initFlutter();
  await initHiveAdapters();
}

Future<void> initHiveAdapters() async {
  Hive.registerAdapter(UserAdapter());
  await Hive.openBox<User>(HiveTableConstants.userTableName);

  Hive.registerAdapter(SinglePersonScoreModelAdapter());
  await Hive.openBox<SinglePersonScoreModel>(
      HiveTableConstants.scoreBoardTableName);

  Hive.registerAdapter(SingleWordModelAdapter());
  await Hive.openBox<SingleWordModel>(
      HiveTableConstants.singleWordModelTableName);

  Hive.registerAdapter(WordSetModelAdapter());
  await Hive.openBox<WordSetModel>(HiveTableConstants.wordSetTableName);
}
