// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:voacl_lab/app/core/data/models/dart_data_models/target_achivement_donut_data_model.dart';
import 'package:voacl_lab/app/core/widget/donut_chart/donut_cart_function/donut_cart_helper_functions.dart';
import 'package:voacl_lab/app/core/widget/donut_chart/donut_chart_helper/info_view_section.dart';
import 'package:voacl_lab/app/core/widget/donut_chart/donut_chart_helper/target_view_section.dart';

class DonutTileContainer extends StatelessWidget {
  TargetDonutStatModel targetAchievementCount;
  DonutTileContainer({
    Key? key,
    required this.targetAchievementCount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late List<DonutChartDataModel> donutChartData = [
      DonutChartDataModel(
        'RED',
        getAchievementPercentage(
          targetAchievementCount.target,
          targetAchievementCount.achievement,
        ),
      ),
      DonutChartDataModel(
        'GRAY',
        getTargetPercentage(
          targetAchievementCount.target,
          targetAchievementCount.achievement,
        ),
      ),
    ];

    return Column(
      children: [
        TargetViewSection(
          chartData: donutChartData,
          targetAchievementData: targetAchievementCount,
        ),
        SizedBox(height: 10),
        InfoViewSection(
          targetAchievementData: targetAchievementCount,
        ),
      ],
    );
  }
}
