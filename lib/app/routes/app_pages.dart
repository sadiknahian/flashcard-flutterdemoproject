// ignore_for_file: constant_identifier_names

import 'package:get/get.dart';
import 'package:voacl_lab/app/modules/learn/bindings/learn_view_binding.dart';
import 'package:voacl_lab/app/modules/learn/views/learn_view.dart';

import '../modules/home/bindings/home_view_binding.dart';
import '../modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeViewBinding(),
    ),
    GetPage(
      name: _Paths.LEARN,
      page: () => LearnView(),
      binding: LearnViewBinding(),
    ),
  ];
}
