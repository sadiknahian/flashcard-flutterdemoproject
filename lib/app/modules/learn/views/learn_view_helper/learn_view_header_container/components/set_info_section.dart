import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voacl_lab/app/modules/learn/controllers/learn_view_controller.dart';

class SetInfoSection extends StatelessWidget {
  final LearnViewController controller;
  const SetInfoSection({
    required this.controller,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Obx(
          () => SetNumberContainer(title: 'Word', number: controller.wordNumber.value.toString()),
        ),
      ],
    );
  }
}

class SetNumberContainer extends StatelessWidget {
  final String title;
  final String number;
  const SetNumberContainer({required this.title, required this.number, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Material(
          elevation: 1.3,
          borderRadius: BorderRadius.circular(10),
          child: Container(
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              color: Colors.white.withOpacity(0.05),
            ),
            child: Text(
              '$title : $number',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
