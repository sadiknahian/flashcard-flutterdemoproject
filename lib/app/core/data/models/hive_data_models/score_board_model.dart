import 'package:hive/hive.dart';
import 'package:voacl_lab/app/core/data/hive_table_constant.dart';

part 'score_board_model.g.dart';

@HiveType(typeId: HiveTableConstants.scoreBoardTypeId)
class SinglePersonScoreModel extends HiveObject {
  @HiveField(0)
  int id = 0;

  @HiveField(1, defaultValue: '')
  String name = '';

  @HiveField(2, defaultValue: -1)
  double score = 0;

  @HiveField(3, defaultValue: -1)
  int position = 0;

  SinglePersonScoreModel({
    required this.name,
    required this.score,
    required this.position,
  });

  SinglePersonScoreModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    score = json['score'];
    position = json['position'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['score'] = score;
    data['position'] = position;
    return data;
  }
}
