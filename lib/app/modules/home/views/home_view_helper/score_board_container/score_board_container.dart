import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:voacl_lab/app/core/widget/data_card/white_data_card.dart';
import 'package:voacl_lab/app/modules/home/controllers/home_view_controller.dart';
import 'package:voacl_lab/app/modules/home/views/home_view_helper/score_board_container/score_board/score_board.dart';

class ScoreBoardContainer extends ConsumerWidget {
  final HomeViewController controller;
  const ScoreBoardContainer({
    Key? key,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      child: WhiteDataCard(
        child: ScoreBoard(
          controller: controller,
        ),
      ),
    );
  }
}
