import 'package:hive/hive.dart';
import 'package:voacl_lab/app/core/data/hive_table_constant.dart';

part 'user.g.dart';

@HiveType(typeId: HiveTableConstants.userTypeId)
class User extends HiveObject {
  @HiveField(0)
  int id = 0;

  @HiveField(1, defaultValue: -1)
  int? userID;

  @HiveField(2, defaultValue: '')
  String userName = '';

  @HiveField(3, defaultValue: '')
  String userMail = '';

  @HiveField(4, defaultValue: -1)
  double lastTestScore = 0;

  @HiveField(5, defaultValue: -1)
  double overAllTestScore = 0;

  @HiveField(6, defaultValue: -1)
  int totalTestTaken = 0;

  @HiveField(7, defaultValue: false)
  bool isPaidUser = false;

  @HiveField(8, defaultValue: '')
  String lastPayDate = '';

  @HiveField(9, defaultValue: true)
  bool isDummyUser = true;

  User({
    required this.userID,
    required this.userName,
    required this.userMail,
    required this.lastTestScore,
    required this.overAllTestScore,
    required this.totalTestTaken,
    required this.isPaidUser,
    required this.lastPayDate,
    required this.isDummyUser,
  });

  User.fromJson(Map<String, dynamic> json) {
    userID = json['user_id'];
    userName = json['user_name'];
    userMail = json['user_mail'];
    lastTestScore = json['last_test_score'];
    overAllTestScore = json['overall_test_score'];
    totalTestTaken = json['total_test_taken'];
    isPaidUser = json['is_paid_user'];
    lastPayDate = json['last_pay_date'];
    isDummyUser = json['is_dummy_user'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['user_id'] = userID;
    data['user_name'] = userName;
    data['user_mail'] = userMail;
    data['last_test_score'] = lastTestScore;
    data['overall_test_score'] = overAllTestScore;
    data['total_test_taken'] = totalTestTaken;
    data['is_paid_user'] = isPaidUser;
    data['last_pay_date'] = lastPayDate;
    data['is_dummy_user'] = isDummyUser;
    return data;
  }
}
